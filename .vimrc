set tabstop=4
set shiftwidth=4
set smarttab

set ai
set cin

set showmatch 
set hlsearch
set incsearch
set ignorecase

set lz

set nu


set ruler

syntax enable

colorscheme desert
set background=dark

set encoding=utf8


set nobackup
set nowb
set noswapfile


set viminfo='10,\"100,:20,%,n~/.viminfo

function! ResCur()
	if line("'\"") <= line("$")
		normal! g`"
		return 1
	endif
endfunction

augroup resCur
	autocmd!
	autocmd BufWinEnter * call ResCur()
augroup END


