#!/bin/bash

cp .gitconfig ~/

cp .git-completion.bash ~/
cp .git-prompt.sh ~/

echo "if [ -f ~/.git-completion.bash ]; then" >> ~/.bash_profile
echo "   . ~/.git-completion.bash" >> ~/.bash_profile
echo "fi" >> ~/.bash_profile

echo "if [ -f ~/.git-prompt.sh ]; then" >> ~/.bash_profile
echo "   . ~/.git-prompt.sh" >> ~/.bash_profile
echo "fi" >> ~/.bash_profile

. ~/.git-completion.bash
. ~/.git-prompt.sh

git config --global core.editor vim
