PS1='\[\e[0;32m\]\u\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[1;32m\]\$\[\e[m\] \[\e[0;38m\]'




alias ll='ls -lah'
alias ls='ls --color'
alias mc='mc -cu'
alias ..='cd ..'
alias ...='cd ../..'
alias ack='ack-grep'
alias diff='colordiff -u'
alias less='less -R'
alias watch='watch --color'

export PROMPT_COMMAND='history -a'

export PAGER="less"
export EDITOR="vim"
export VISUAL="vim"







