#!/usr/bin/perl

use 5.012;
use strict;
use warnings;

use FindBin;

chdir $FindBin::Bin;

say "Linux enveriment init";


main();
exit();


sub main {
	bash_init();
	vim_init();
	git_init();
	fish_init();
}

sub bash_init {
	say "Bash init..";
	`./bash-init.sh`;
}

sub vim_init {
	say "Vim init..";
	`./vim-init.sh`;
}

sub git_init {
	say "Git init...";
	`./git-init.sh`;
}

sub fish_init {
	say "Try to chage default shell to fish";
	`chsh -s \$(which fish)`;
	
	
	my $fish_config = '
		if test -f ~/.git-completion.bash
			. ~/.git-completion.bash
		end

		if test -f ~/.git-prompt.sh
			. ~/.git-prompt.sh
		end
	';
	
	my $dir = home_dir();
	
	if (-e $dir . '/.config/fish/') {
		my $file = $dir . '/.config/fish/config.fish';
		if (-e $file) {
			say "$file exists. It will overwritten";
		}
		`cp ./config.fish $file`;
	}
}

sub home_dir {
	return (getpwuid $>)[7];
}


1;


